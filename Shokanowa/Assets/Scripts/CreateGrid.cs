﻿using UnityEngine;
using System.Collections;

public class CreateGrid : MonoBehaviour 
{
	public GameObject tile;
	public Vector2 gridSize;
	public Vector2 offset;
	// Use this for initialization
	private void Start () 
	{
		createGrid();
	}
	
	// Update is called once per frame
	private void Update () 
	{
	
	}
	
	private void createGrid()
	{
		for(int x = 0; x < gridSize.x; x++)
		{
			for (int y = 0; y < gridSize.y; y++)
			{
				createTile(x, y);
			}
		}
	}
	
	private void createTile(int tileX, int tileY)
	{
		GameObject tilePiece = (GameObject)Instantiate(tile) as GameObject;
		Vector3 tilePosition = new Vector3(tileX * offset.x, tileY * offset.y, 0);
		tilePiece.transform.position = tilePosition;
		tilePiece.GetComponent<TileStats>().setTileStats(tilePosition, new Vector2(tileX, tileY));
	}
}
