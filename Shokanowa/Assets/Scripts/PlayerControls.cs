﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour 
{
	public LayerMask floor;
	public Ray camRay;
	public float camLength;
	public GameObject UI_Menu;
	public Vector2 uiOffest;
	private GameObject ui;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (ui == null)
			{
				rayCast();
			}
		}
	}
	
	private void rayCast()
	{
		camRay = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		RaycastHit rayHit;
		if (Physics.Raycast(camRay, out rayHit, camLength, floor))
		{
			TileStats tileStat = rayHit.collider.GetComponent<TileStats>();
			tileStat.setTileStatus(0);
			ui = (GameObject)Instantiate(UI_Menu);
			ui.GetComponent<PlayerUIControls>().tile = tileStat;
			Vector3 uiPos = rayHit.point;
			uiPos.z = -5f;
			uiPos.x += uiOffest.x;
			uiPos.y += uiOffest.y;
			ui.transform.position = uiPos;
		}
	}
}
