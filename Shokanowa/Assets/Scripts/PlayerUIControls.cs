﻿using UnityEngine;
using System.Collections;

public class PlayerUIControls : MonoBehaviour 
{
	public TileStats tile;
	public ResourceManager resMang;
	public float demolishEnergyCost;
	public float solarEnergyBuildCost;
	public float treebuildCost;
	public float sellAmount;
	public float buyLand;
	public AudioClip buzz;
	public AudioSource audoSOurce;
	
	private void Awake()
	{
		resMang = GameObject.FindGameObjectWithTag("GameCtrl").GetComponent<ResourceManager>();
		audoSOurce = resMang.gameObject.GetComponent<AudioSource>();
	}
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	//sell
		//detect amount through scroll
	//build
	//buy
	
	public void demolish()
	{
		//alterCurrentValue();
		if (resMang.energy > demolishEnergyCost )
		{
			if (tile.owned)
			{
				resMang.modifiyEnergy(-demolishEnergyCost);
				tile.alterCurrentTile(TILE_TYPE.BREAK);
			}
		}
		else
		{
			audoSOurce.PlayOneShot(buzz);
		}
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	public void buildSolar()
	{
		if (resMang.energy > solarEnergyBuildCost)
		{
			if (tile.owned)
			{
				resMang.modifiyEnergy(-solarEnergyBuildCost);
				tile.alterCurrentTile(TILE_TYPE.SOLAR_PP);
			}
		}
		else
		{
			audoSOurce.PlayOneShot(buzz);
		}
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	public void buildTree()
	{
		if (resMang.energy > treebuildCost)
		{
			if (tile.owned)
			{
				resMang.modifiyEnergy(-treebuildCost);	
				tile.alterCurrentTile(TILE_TYPE.TREE);
			}
		}
		else
		{
			audoSOurce.PlayOneShot(buzz);
		}
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	public void sell()
	{
		print("build sell");
		if (tile.owned)
		{
			resMang.modifiyEnergy(sellAmount);
			TileStats tileStats = tile.GetComponent<TileStats>();	
			tileStats.owned = false;
			tileStats.turnToWaste();
		}
		else
		{
			audoSOurce.PlayOneShot(buzz);
		}
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	public void buy()
	{
		print("build buy");
		if (resMang.energy > buyLand && !tile.owned)
		{
			resMang.modifiyEnergy(-buyLand);
			TileStats tileStats = tile.GetComponent<TileStats>();	
			tileStats.setTileStatus(1);
			tileStats.owned = true;
		}
		else
		{
			audoSOurce.PlayOneShot(buzz);
		}
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	public void cancel()
	{
		returnSelectStatus();
		Destroy(gameObject);
	}
	
	private void returnSelectStatus()
	{
		TileStats tileStats = tile.GetComponent<TileStats>();
		if (tileStats.owned == false)
		{
			tileStats.setTileType(tile.ENUM_TILE_TYPE);
		}
		else
		{
			tileStats.setTileStatus(1);
		}
	}
}
