﻿using UnityEngine;
using System.Collections;

public enum TILE_TYPE
{	
	NUCLEAR_PP,
	NUCLEAR_WASTE,
	TREE,
	SOLAR_PP,
	NORMAL,
	BREAK,	
	NONE
};

[System.Serializable]
public struct TileMaterial
{
	public Material nuclear_PP, nuclearWaste, tree, solar_PP, normal, border, breakDown;
	public float nuclearD, nuclearWasteD, treeD, solarE;
}

public class TileStats : MonoBehaviour 
{
	public Vector3 tilePosition;
	public Vector2 tileId;
	public float turnToWasteTimer;
	public TILE_TYPE ENUM_TILE_TYPE;
	public TileMaterial tileMat;
	[HideInInspector]
	public bool owned = false;
	// Use this for initialization
	private ResourceManager resMang;
	public GameObject childRenderer;
	
	private void Start()
	{
	
	}
	
	private void Awake () 
	{
		//GetComponent<Renderer>().material = tileMat.tree;
		resMang = GameObject.FindGameObjectWithTag("GameCtrl").GetComponent<ResourceManager>();
	}
	
	public void setTileStatus(int tileStatus)
	{
		Renderer childRend = childRenderer.GetComponent<Renderer>();
		if (childRend != null)
		{
			switch(tileStatus)
			{
				case 0:
					childRend.material.color = Color.blue;
					break;
				case 1:
					if (owned)
					{
						childRend.material.color = Color.green;
					}
					else
					{
						childRend.material.color = Color.red;	
					}
					break;
				case 2:
					childRend.material.color = Color.red;
					break;
				default:
					childRend.material.color = Color.white;
					break;
			}
		}
	}
	
	public void setTileStats(Vector3 position, Vector2 ID)
	{
		tilePosition = position;
		tileId = ID;
		determineTileType();
	}
	
	private void determineTileType()
	{
		float randomNumber = Random.Range(0f, 100f);
		//tileType = (TILE_TYPE)Random.Range(0, 4);
		if (randomNumber > 20f)
		{
			//spawn waste tiles
			ENUM_TILE_TYPE = (TILE_TYPE)Random.Range(0, 2);	
		}
		else if (randomNumber >= 15f)
		{
			//spawn nature tiles
			ENUM_TILE_TYPE = (TILE_TYPE)Random.Range(3, 3);
			owned = true;
		}
		else
		{
			ENUM_TILE_TYPE = TILE_TYPE.NORMAL;
		}
		setTileType(ENUM_TILE_TYPE);
	}
	
	public void setTileType(TILE_TYPE TYPE)
	{
		//resMang = GameObject.FindGameObjectWithTag("GameCtrl").GetComponent<ResourceManager>();
		switch(TYPE)
		{
			case TILE_TYPE.NUCLEAR_PP:
				GetComponent<Renderer>().material = tileMat.nuclear_PP;
				resMang.modifyDecayRate(tileMat.nuclearD);
				setTileStatus(2);
				break;
			case TILE_TYPE.NUCLEAR_WASTE:
				GetComponent<Renderer>().material = tileMat.nuclearWaste;
				resMang.modifyDecayRate(tileMat.nuclearWasteD);
				setTileStatus(2);
				break;
			case TILE_TYPE.TREE:
				GetComponent<Renderer>().material = tileMat.tree;
				resMang.modifyDecayRate(-tileMat.treeD);
				setTileStatus(3);
				break;
			case TILE_TYPE.SOLAR_PP:
				GetComponent<Renderer>().material = tileMat.solar_PP;
				resMang.modifyEnergyRate(tileMat.solarE);
				setTileStatus(1);
				break;
			case TILE_TYPE.NORMAL:
				GetComponent<Renderer>().material = tileMat.normal;
				setTileStatus(3);
				break;
			case TILE_TYPE.BREAK:
				GetComponent<Renderer>().material = tileMat.breakDown;
				setTileStatus(3);
				break;
		default:
				setTileStatus(3);
				GetComponent<Renderer>().material = tileMat.border;
				break;
		}
	}
	
	public void alterCurrentTile(TILE_TYPE changeType)
	{
		switch(ENUM_TILE_TYPE)
		{
			case TILE_TYPE.NUCLEAR_PP:
				resMang.modifyDecayRate(-tileMat.nuclearD);
				break;
			case TILE_TYPE.NUCLEAR_WASTE:
				GetComponent<Renderer>().material = tileMat.nuclearWaste;
				resMang.modifyDecayRate(-tileMat.nuclearWasteD);
				break;
			case TILE_TYPE.TREE:
				GetComponent<Renderer>().material = tileMat.tree;
				resMang.modifyDecayRate(tileMat.treeD);
				break;
			case TILE_TYPE.SOLAR_PP:
				GetComponent<Renderer>().material = tileMat.solar_PP;
				resMang.modifyEnergyRate(-tileMat.solarE);
				break;
			case TILE_TYPE.NORMAL:
				GetComponent<Renderer>().material = tileMat.normal;
				break;
			case TILE_TYPE.BREAK:
				GetComponent<Renderer>().material = tileMat.breakDown;
				break;
			default:
				GetComponent<Renderer>().material = tileMat.border;
				break;
		}
		ENUM_TILE_TYPE = changeType;
		setTileType(ENUM_TILE_TYPE);
	}
	
	public IEnumerator turnToWaste()
	{
		float timer = turnToWasteTimer;
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		ENUM_TILE_TYPE = TILE_TYPE.NUCLEAR_WASTE;
		setTileType(ENUM_TILE_TYPE);
	}
}
