﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour 
{
	public float cameraSpeed;
	public float screenOffset;
	public GameObject botPositionClamp;
	public GameObject topPositionClamp;
	
	private void Start()
	{
	
	}
	
	private void Update()
	{
		//clampCameraPosition();
	}
	
	private void LateUpdate()
	{
		checkMousePos();
	}
	
	private void clampCameraPosition()
	{
		if (botPositionClamp.transform.position.x < Camera.main.transform.position.x)
		{
			Vector3 cameraPos = Camera.main.transform.position;
			cameraPos.x = botPositionClamp.transform.position.x;
			Camera.main.transform.position = cameraPos;
		}
		else if (topPositionClamp.transform.position.x > Camera.main.transform.position.x)
		{
			Vector3 cameraPos = Camera.main.transform.position;
			cameraPos.x = topPositionClamp.transform.position.x;
			Camera.main.transform.position = cameraPos;
		}
		
		if (botPositionClamp.transform.position.y < Camera.main.transform.position.y)
		{
			Vector3 cameraPos = Camera.main.transform.position;
			cameraPos.y = botPositionClamp.transform.position.y;
			Camera.main.transform.position = cameraPos;
		}
		else if (topPositionClamp.transform.position.y > Camera.main.transform.position.y)
		{
			Vector3 cameraPos = Camera.main.transform.position;
			cameraPos.y = topPositionClamp.transform.position.y;
			Camera.main.transform.position = cameraPos;
		}
	}
	
	private void checkMousePos()
	{
		float botScreen = screenOffset;
		float topScreen = Screen.height - screenOffset;
		float leftScreen = screenOffset;
		float rightScreen = Screen.width - screenOffset;
		if (botScreen > Input.mousePosition.y || Input.GetKey(KeyCode.S))
		{
			moveCamera(Vector3.down);
		}
		else if (topScreen < Input.mousePosition.y || Input.GetKey(KeyCode.W))
		{
			moveCamera(Vector3.up);
		}
		
		if (Input.mousePosition.x < leftScreen || Input.GetKey(KeyCode.A))
		{
			moveCamera(Vector3.left);
		}
		else if (Input.mousePosition.x >rightScreen || Input.GetKey(KeyCode.D))
		{
			moveCamera(Vector3.right);
		}
	}
	
	private void moveCamera(Vector3 direction)
	{
		Camera.main.transform.Translate(direction * cameraSpeed * Time.deltaTime);
	}	
}
