﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour 
{
	[Header("Money")]
	public float money;
	public float sellModifier;
	public float moneyCap;
	[Header("Energy")]
	public float energy;
	public float energyRate;
	public float energyCap;
	[Header("Energy")]
	public float decay;
	public float decayRate;
	public float decayCap;
	
	[Header("UI")]
	public Text decayUI;
	public Text energyUI;
	private void debugControls()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			modifiyEnergy(10);
		}
		
		if (Input.GetKey(KeyCode.R))
		{
			increaseEnergy();
		}
	}
	
	
	private void Update()
	{
		increaseEnergy();
		decayEnvironment();
	}
	
	public void decayEnvironment()
	{
		decay += decayRate * Time.deltaTime;
		decayUI.text = "Decay: " + (int)decay;
		if (decay >= decayCap)
		{
			loseGame();
		}
		else if (decayRate <= 0)
		{
			winGame();
		} 
	}
	
	public void sellEnergy(float soldEnergy)
	{
		money +=  (soldEnergy * sellModifier);
		print(soldEnergy + "Sell Modifier" + sellModifier);
		if (money >= moneyCap)
		{
			money = moneyCap;
		}
	}
	
	public void modifiyEnergy(float amount)
	{
		if ((energy + amount) >= energyCap)
		{
			energy = energyCap;
		}
		else if (energy < 0)
		{
			energy = 0;
		}
		else
		{
			energy += amount;
		}
	}
	
	private void increaseEnergy()
	{
		energyUI.text = "Energy: " + (int)energy;
		float energyR = energyRate * Time.deltaTime;
		modifiyEnergy(energyR);
	}
	
	public void modifyEnergyRate(float amount)
	{
		energyRate += amount;
	}
	
	public void modifyDecayRate(float amount)
	{
		decayRate += amount;
		if (decayRate <= 0)
		{
			winGame();
		} 
	}
	
	private void winGame()
	{
		PlayerPrefs.SetString("Cond", "You win");
		Application.LoadLevel(2);
	}
	
	private void loseGame()
	{
		PlayerPrefs.SetString("Cond", "You lose");
	}
}
