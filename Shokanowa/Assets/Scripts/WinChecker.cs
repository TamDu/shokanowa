﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinChecker : MonoBehaviour 
{
	public Text winTxt;
	// Use this for initialization
	private void Start () 
	{
		string result = PlayerPrefs.GetString("Cond", "You Win");
		winTxt.text = result;
	}
}
