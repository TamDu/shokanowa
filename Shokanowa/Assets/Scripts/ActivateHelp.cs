﻿using UnityEngine;
using System.Collections;

public class ActivateHelp : MonoBehaviour {

	public GameObject[] mainCanvus;
	public GameObject[] subCanvus;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void displayHelpStatus()
	{
		bool status = mainCanvus[0].activeSelf; 
		for (int i = 0; i < mainCanvus.Length; i++)
		{
			mainCanvus[i].SetActive(!status);
		}
		for (int i = 0; i < subCanvus.Length; i++)
		{	
			subCanvus[i].SetActive(status);
		}
	}
}
